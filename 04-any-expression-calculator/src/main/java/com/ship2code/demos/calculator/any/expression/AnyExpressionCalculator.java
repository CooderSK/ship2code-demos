package com.ship2code.demos.calculator.any.expression;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.Scanner;

public class AnyExpressionCalculator {

    public static void main(String[] args) {
        showWelcomeMessage();
        commandLoop();
        showExitMessage();
    }

    // Shows welcome messages
    public static void showWelcomeMessage() {
        System.out.println("Welcome to the any expression calculator program");
        System.out.println("Enter the expression or 'exit' to terminate the program");
        System.out.println("This calculator can take multiple expressions");
        System.out.println("Example expression: 5 * (5 -2.5)\n");
    }

    // Shows exit message
    public static void showExitMessage() {
        System.out.println("Goodbye, come back soon ^-^\n");
    }

    // Infinite loop of command processing
    public static void commandLoop() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("/> ");
            String command = scanner.nextLine();

            boolean isExitOrStop = processCommand(command);

            if (isExitOrStop) {
                break;
            }

            System.out.println("");
        }
    }

    // Process the command / calculate result
    // Returns true - if command is stop or exit, otherwise return false
    // Command can be "exit", "stop" or expression like "5 * (10 + 5.2)
    // Expression can contain multiple numbers and operators
    public static boolean processCommand(String command) {
        if (command.equals("exit") || command.equals("stop")) {
            return true;
        }

        calculateExpression(command);

        return false;
    }

    // Calculate expression and show result
    public static void calculateExpression(String expression) {
        double result;

        try {
            Expression ex = new ExpressionBuilder(expression).build();
            result = ex.evaluate();
        } catch (Exception e) {
            System.out.println("An unexpected error occured. " + e.getMessage());
            return;
        }

        System.out.println(expression + " = " + result);
    }

}

