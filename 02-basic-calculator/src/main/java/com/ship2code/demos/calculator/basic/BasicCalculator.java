package com.ship2code.demos.calculator.basic;

import java.util.Scanner; // Import of the scanner class

public class BasicCalculator { // Main class of the program
    public static void main(String[] args) { // Main function - start of the program
        Scanner scanner = new Scanner(System.in); // Created an object of the scanner class which will help to load the text from console.

        System.out.println("Num1: "); // Print to console
        String text1 = scanner.nextLine(); // Get line from console using scanner, and save it to variable
        int number1 = Integer.parseInt(text1); // Convert text1 var into integer variable

        System.out.println("Num2: ");
        String text2 = scanner.nextLine();
        int number2 = Integer.parseInt(text2);

        System.out.println("Operator: ");
        String operator = scanner.nextLine();

        int result; // numeric variable for result

        if (operator.equals("+")) {
            result = number1 + number2;
        } else if (operator.equals("-")) {
            result = number1 - number2;
        } else if (operator.equals("*")) {
            result = number1 * number2;
        } else if (operator.equals("/")) {
            result = number1 / number2;
        } else {
            System.out.println("Wrong character");
            return; // If conditions not met, main function/program will stop
        }

        System.out.println(number1 + " " + operator + " " + number2 + " = " + result); // Result printed to console
    }
}
