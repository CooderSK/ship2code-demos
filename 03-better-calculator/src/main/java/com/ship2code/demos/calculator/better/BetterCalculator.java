package com.ship2code.demos.calculator.better;

// Import scanner to the class
import java.util.Scanner;

// Useful keyboard shortcuts:
// Use CTRL + click to go into the function definition
// Use ALT + left arrow to go back with cursor

public class BetterCalculator {
    // Main function - start of the program
    public static void main(String[] args) {
        showWelcomeMessage();
        commandLoop();
        showExitMessage();
    }

    // Shows welcome messages
    public static void showWelcomeMessage() {
        System.out.println("Welcome to the better calculator program");
        System.out.println("Enter the expression or 'exit' to terminate the program");
        System.out.println("Example expression: 5 * -2.5\n");
    }

    // Shows exit message
    public static void showExitMessage() {
        System.out.println("Goodbye, come back soon ^-^\n");
    }

    // Infinite loop of command processing
    public static void commandLoop() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("/> ");
            String command = scanner.nextLine();

            boolean isExitOrStop = processCommand(command);

            if (isExitOrStop) {
                break;
            }

            System.out.println("");
        }
    }

    // Process the command / calculate result
    // Returns true - if command is stop or exit, otherwise return false
    // Command can be "exit", "stop" or expression like "5 + -5.248"
    // Elements in expression must be separated by space
    // Expression must contain 2 numbers and one operator between
    public static boolean processCommand(String command) {
        if (command.equals("exit") || command.equals("stop")) {
            return true;
        }

        String[] splitted = command.split(" ");

        if (splitted.length != 3) {
            System.out.println("Invalid expression, must contain 2 number and 1 separate operator in between and elements must be separated by space!");
            return false;
        }

        // In splitted array will be 3 Strings
        // 0 - "5"
        // 1 - "+"
        // 2 - "-5.248"

        double num1 = Double.parseDouble(splitted[0]);
        double num2 = Double.parseDouble(splitted[2]);

        if (splitted[1].length() != 1) {
            System.out.println("Invalid expression, must contain 2 number and 1 separate operator in between and elements must be separated by space!");
            return false;
        }

        char operator = splitted[1].charAt(0);

        calculateExpression(num1, operator, num2);

        return false;
    }

    // Calculate expression and show result
    public static void calculateExpression(double num1, char operator, double num2) {
        double result;

        if (operator == '+') {
            result = num1 + num2;
        } else if (operator == '-') {
            result = num1 - num2;
        } else if (operator == '*') {
            result = num1 * num2;
        } else if (operator == '/') {
            result = num1 / num2;
        } else if (operator == '^') {
            // Calculate mathematical power of two numbers
            result = Math.pow(num1, num2);
        } else {
            System.out.println("Unsupported Operator, only + - * / ^ can be used");

            // Stop execution of this function
            return;
        }

        // Print result to console
        System.out.println(num1 + " " + operator + " " + num2 + " = " + result);
    }
}
